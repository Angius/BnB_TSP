﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB_TSP
{
    public class Matrix
    {
        public List<Point> ListOfPoints {get; set;}
        public double[,] OutMatrix { get; set; }

        // Matrix object
        public Matrix (List<Point> listOfPoints)
        {
            ListOfPoints = listOfPoints;

            OutMatrix = new double[listOfPoints.Count, listOfPoints.Count];

            for (int row = 0; row < listOfPoints.Count; row++)
            {
                for (int col = 0; col < listOfPoints.Count; col++)
                {
                    if (col == row)
                    {
                        OutMatrix[row, col] = Double.NaN;
                    }
                    else
                    {
                        OutMatrix[row, col] = Point.Distance(listOfPoints[row], listOfPoints[col]);
                    }
                }
            }
        }

        // Matrix object II
        public Matrix (double[,] matrix, List<Point> listOfPoints)
        {
            ListOfPoints = listOfPoints;

            OutMatrix = matrix;
        }

        // Method to replace rows and columns with NaN
        public static Matrix RemoveRowsCols (Matrix matrixToChange, Point from, Point to)
        {
            Matrix outMatrix = matrixToChange;

            for (int col = 0; col < matrixToChange.ListOfPoints.Count; col++)
            {
                for (int row = 0; row < matrixToChange.ListOfPoints.Count; row++)
                {
                    if (from.ID == row)
                    {
                        outMatrix.OutMatrix[row, col] = Double.NaN;
                    }

                    if (to.ID == col)
                    {
                        outMatrix.OutMatrix[row, col] = Double.NaN;
                    }
                }
            }

            outMatrix.OutMatrix[to.ID, from.ID] = Double.NaN;

            return outMatrix;
        }

        // Method to reduce matrix
        public static Tuple<Matrix, double> Reduce (Matrix matrixToReduce)
        {
            double rowReduction = 0;
            double colReduction = 0;
            Matrix outMatrix = matrixToReduce;

            // Reduce by row
            for (int row = 0; row < outMatrix.ListOfPoints.Count; row++)
            {
                double[] tmpRow = new double[outMatrix.ListOfPoints.Count];
                double tmpRowMin = 0.0;

                // Save a row to a new array
                for (int col = 0; col < outMatrix.ListOfPoints.Count; col++)
                {
                    tmpRow[col] = outMatrix.OutMatrix[row, col];
                }

                // Create a new array without any NaN values
                var nonNanRow= tmpRow.Where(x => !double.IsNaN(x));

                // Check if the row should be reduced
                if (nonNanRow.Count() == 0 || nonNanRow.Min() == 0)
                {
                    tmpRowMin = 0;
                }
                else
                {
                    tmpRowMin = nonNanRow.Min();
                }

                // Add resulting reduction to total row reduction
                rowReduction += tmpRowMin;

                // Update the matrix
                for (int col = 0; col < outMatrix.ListOfPoints.Count; col++)
                {
                    outMatrix.OutMatrix[row, col] -= tmpRowMin;
                }
            }


            // Reduce by column
            for (int col = 0; col < outMatrix.ListOfPoints.Count; col++)
            {
                double[] tmpCol = new double[outMatrix.ListOfPoints.Count];
                double tmpColMin = 0.0;

                // Save a column to a new array
                for (int row = 0; row < outMatrix.ListOfPoints.Count; row++)
                {
                    tmpCol[col] = outMatrix.OutMatrix[row, col];
                }

                // Create a new array without any NaN values
                var nonNanCol = tmpCol.Where(x => !double.IsNaN(x));

                // Check if the row should be reduced
                if (nonNanCol.Count() == 0 || nonNanCol.Min() == 0)
                {
                    tmpColMin = 0;
                }
                else
                {
                    tmpColMin = nonNanCol.Min();
                }

                // Add resulting reduction to total column reduction
                colReduction += tmpColMin;

                // Update the matrix
                for (int row = 0; row < outMatrix.ListOfPoints.Count; row++)
                {
                    outMatrix.OutMatrix[row, col] -= tmpColMin;
                }
            }

            // Return the tuple
            Tuple<Matrix, double> outTuple = new Tuple<Matrix, double>(outMatrix, rowReduction + colReduction);

            return outTuple;
        }
    }
}
