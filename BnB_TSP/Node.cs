﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB_TSP
{
    public class Node
    {
        public int ID { get; set; }
        public Point Point { get; set; }
        public Node Parent { get; set; }
        public Point[] Children { get; set; }
        public double Value { get; set; }
        public Matrix Matrix { get; set; }

        public Node(int id, Point point, Node parent, Point[] children, double value, Matrix matrix)
        {
            ID = id;
            Point = point;
            Parent = parent;
            Children = children;
            Value = value;
            Matrix = matrix;
        }

        public static Node Min(List<Node> nodes)
        {
            List<double> values = new List<double>();

            for (int i = 0; i < nodes.Count(); i++)
            {
                values.Add(nodes[i].Value);
            }

            int lowestIndex = values.IndexOf(values.Min());

            return nodes[lowestIndex];
        }
    }
}
