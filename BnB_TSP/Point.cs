﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB_TSP
{
    public class Point
    {
        public int ID { get; set; }
        public int Xcoord { get; set; }
        public int Ycoord { get; set; }
        public string Name { get; set; }
 
        public Point (int id, int xCoord, int yCoord, string name)
        {
            ID = id;
            Xcoord = xCoord;
            Ycoord = yCoord;
            Name = name;
        }

        public static double Distance (Point a, Point b)
        {
            double bracketA = Math.Pow(a.Xcoord - b.Xcoord, 2);
            double bracketB = Math.Pow(a.Ycoord - b.Ycoord, 2);

            return Math.Sqrt(bracketA + bracketB);
        }
    }

}
