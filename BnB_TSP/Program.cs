﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB_TSP
{
    class Program
    {
        static void Main(string[] args)
        {
            List <Point> listOfPoints = new List<Point>();

            Console.WriteLine(@"
            ╔══════════════════════════════════╗
            ║    TRAVELLING SALESMAN PROBLEM   ║
            ║ Using Branch and Bound algorithm ║
            ╠══════════════════════════════════╣
            ║         Łukasz Kondracki         ║
            ╠══════════════════════════════════╣
            ║ Input city coordinates as A,X,Y  ║
            ║ Confirm each entry with [ENTER]  ║
            ║ Leave input empty to calculate   ║
            ╚══════════════════════════════════╝");

            bool inputControl = true;
            int loopCounter = 0;

            // Read user input
            while (inputControl)
            {
                string inputString = Console.ReadLine();

                if (inputString == "")
                {
                    inputControl = false;
                }
                else
                {
                    string[] tmpPointStr = inputString.Split(',');

                    Point tmpPoint = new Point(loopCounter, Int32.Parse(tmpPointStr[1]), Int32.Parse(tmpPointStr[2]), tmpPointStr[0]);

                    listOfPoints.Add(tmpPoint);
                }

                loopCounter++;
            }

            // Put all input on screen
            foreach(Point s in listOfPoints)
            {
                Console.WriteLine("[" + s.ID + "] " + "City " + s.Name + " exists on coordinates " + s.Xcoord + ", " + s.Ycoord);
                Console.WriteLine();
            }

            Matrix StartMatrix = new Matrix(listOfPoints);

            for(int i = 0; i < StartMatrix.ListOfPoints.Count; i++)
            {
                for (int j = 0; j < StartMatrix.ListOfPoints.Count; j++)
                {
                    Console.Write(StartMatrix.OutMatrix[i, j] + " // ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();


            // Calculate first node

            // Create array of children
            Point[] startNodeChildren = new Point[listOfPoints.Count()-1];
            for(int i = 0; i < startNodeChildren.Length; i++)
            {
                startNodeChildren[i] = listOfPoints[i + 1];
            }

            // Create a tuple
            Tuple<Matrix, double> startNodeTuple = Matrix.Reduce(StartMatrix);

            Node startNode = new Node(0, listOfPoints[0], null, startNodeChildren, startNodeTuple.Item2, startNodeTuple.Item1);

            // Create a list of all nodes and a list of top-level nodes
            List<Node> allNodesList = new List<Node>();
            List<Node> topLevelNodesList = new List<Node>();

            // Initialize all nodes list
            int nodeIdCounter = 1;

            allNodesList.Add(startNode);

            foreach(Point p in startNode.Children)
            {
                // Calculate node children
                Point[] nodeChildren = startNode.Children;

                nodeChildren.ToList().Remove(startNode.Point);
                nodeChildren.ToArray();

                // Create node tuple
                Tuple<Matrix, double> nodeTuple = Matrix.Reduce(Matrix.RemoveRowsCols(startNode.Matrix, startNode.Point, p));

                // Add node to all nodes list
                allNodesList.Add(new Node(nodeIdCounter, p, startNode, nodeChildren, nodeTuple.Item2, nodeTuple.Item1));

                // Increase counter
                nodeIdCounter++;
            }

            // Initialize top-level nodes list
            topLevelNodesList = allNodesList;
            topLevelNodesList.Remove(startNode);


            // Perform calculations proper
            bool algorithmLoopControl = true;

            while (algorithmLoopControl)
            {
                // Get lowest value top-level node
                Node currentNode = Node.Min(topLevelNodesList);

                Console.WriteLine(currentNode.ID + ", " + currentNode.Point.Name);

                // Calculate its children
                foreach (Point p in currentNode.Children)
                {
                    Console.Write(p.Name + ", ");

                    // Save node matrix
                    Matrix childMatrix = currentNode.Matrix;

                    // Cross out matrix
                    Matrix childCrossedMatrix = Matrix.RemoveRowsCols(childMatrix, currentNode.Parent.Point, currentNode.Point);

                    // Reduce matrix
                    Tuple<Matrix, double> childTuple = Matrix.Reduce(childCrossedMatrix);

                    // Calculate node children
                    Point[] childChildren = currentNode.Children;

                    List<Point> tmpChildList = currentNode.Children.ToList();
                    Point tmpPointToRemove = p;
                    tmpChildList.Remove(tmpPointToRemove);

                    childChildren = tmpChildList.ToArray();

                    /*
                    for (int i = 0; i < childChildren.Length; i++)
                    {
                        childChildren[i] = currentNode.Children[i + 1];
                    }
                    */

                    // Add node to all nodes list
                    allNodesList.Add(new Node(nodeIdCounter, p, currentNode, childChildren, childTuple.Item2, childTuple.Item1));

                    //Add node to top-level nodes list
                    topLevelNodesList.Add(new Node(nodeIdCounter, p, currentNode, childChildren, childTuple.Item2, childTuple.Item1));

                    // Increase ID counter
                    nodeIdCounter++;

                }

                // Remove current node from top-level list
                topLevelNodesList.Remove(currentNode);

                if (currentNode.Children.Count() == 0)
                {
                    algorithmLoopControl = false;
                }

                Console.WriteLine();
            }

            // Save path
            Point[] shortestPath = new Point[listOfPoints.Count()];
            Node tempNode = allNodesList[allNodesList.Count() - 1];

            for (int i = shortestPath.Count() - 1; i >= 0; i--)
            {
                shortestPath[i] = tempNode.Point;

                tempNode = tempNode.Parent;
            }

            // Print shortest path
            for (int i = 0; i < shortestPath.Count(); i++)
            {
                Console.Write(shortestPath[i].Name + " ");
            }

            Console.Read();
        }
    }
}
